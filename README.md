# Query Engine

## Install

```sh
pip install git+https://gitlab.com/ibilon.openit.com/queryengine.git#egg=openit-queryengine
```

Verify if it was installed successfully.

```sh
openit-queryengine --help
```

## Get Standard Queries

```sh
cd "C:\ProgramData\OpeniT\Data"
git clone git@gitlab.com:ibilon.openit.com/queries.git
cd queries
pip install -r requirements.txt
```

## Run

```sh
openit-queryengine --data "C:\ProgramData\OpeniT\Data" --queries "C:\ProgramData\OpeniT\Data\queries"
```

Go to http://localhost:40152/queries/sample to verify if it works.
