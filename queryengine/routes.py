from queryengine import runner
from aiohttp import web
import os

routes  = web.RouteTableDef()
headers = {
    'Content-Encoding': 'gzip',
    'Content-Type':     'text/csv'
}

@routes.get('/queries/{name}')
async def handle(request):
    name = request.match_info.get('name')
    if runner.exists(name):
        filepath = runner.run(name, request.query)
        if not os.path.exists(filepath):
            raise web.HTTPNoContent()
    else:
        raise web.HTTPNotFound()
    return web.FileResponse(filepath, headers=headers)
