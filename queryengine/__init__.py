from queryengine import settings, entrypoint, routes
from aiohttp import web
import argparse
import os

def main():
    args = parse()
    configure(args)
    verify()
    app = web.Application()
    app.add_routes(routes.routes)
    web.run_app(app, port=settings.port)

def run():
    entrypoint.call(main)

def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('--port',
        help='port number (default %s)' % settings.port,
        default=settings.port)
    parser.add_argument('--cache',
        help='cache directory',
        default=settings.cache_dir)
    parser.add_argument('--data',
        help='data directory',
        required=True)
    parser.add_argument('--queries',
        action='append',
        help='query scripts directory',
        required=True)
    parser.add_argument('--maxage',
        help='max age of cache files (default %s)' % settings.max_age,
        default=settings.max_age)
    return parser.parse_args()

def configure(args):
    settings.port        = args.port
    settings.cache_dir   = args.cache
    settings.data_dir    = args.data
    settings.queries_dir = args.queries
    settings.max_age     = int(args.maxage)

def verify():
    assert os.path.exists(settings.data_dir)
    os.makedirs(settings.cache_dir, exist_ok=True)

if __name__ == "__main__":
    run()
