from queryengine import settings
import subprocess
import hashlib
import time
import sys
import os

def run(name, query):
    filepath   = find(name)
    query_args = dict_to_args(query)
    args       = get_runtime(filepath) + [ filepath, '--data', settings.data_dir ] + query_args
    output     = make_output_file(args)
    args       = args + ['--output', output]

    if os.path.exists(output):
        age = get_age(output)
        if age >= settings.max_age:
            try:
                os.remove(output)
            except OSError:
                pass
    
    if not os.path.exists(output):
        subprocess.run(args)

    return output

def dict_to_args(dictionary):
    result = []
    for key in dictionary:
        value = dictionary[key]
        result.append('--' + key)
        result.append(value)
    return result

def hash(array):
    m = hashlib.sha1()
    for value in array:
        m.update(value.encode())
    return m.hexdigest()[0:7]

def make_output_file(array):
    filename = hash(array) + '.csv.gz'
    return os.path.join(settings.cache_dir, filename)

def get_age(filepath):
    stat          = os.stat(filepath)
    last_modified = stat.st_mtime
    now           = time.time()
    age           = now - last_modified
    return age

def find(name):
    dirs      = settings.queries_dir
    exe       = '.exe' if sys.platform == 'win32' else ''
    filenames = [ name + exe, name + '.py' ]
    for dir in dirs:
        for filename in filenames:
            filepath = os.path.join(dir, filename)
            if os.path.exists(filepath):
                return filepath
    return None

def exists(name):
    return find(name) != None

def get_runtime(filepath):
    _, ext = os.path.splitext(filepath)
    python = 'python' if sys.platform == 'win32' else 'python3'
    return [ python ] if ext == '.py' else []
