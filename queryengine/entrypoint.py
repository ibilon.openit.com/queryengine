import logging
import signal
import sys

def init():
    signal.signal(signal.SIGINT, terminate)
    signal.signal(signal.SIGTERM, terminate)
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

def call(main):
    try:
        init()
        main()
    except AssertionError as e:
        logging.error(e)
        sys.exit(1)
    except Exception as e:
        logging.error(e)
        raise e

def terminate(*args):
    sys.exit(0)
