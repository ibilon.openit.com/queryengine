from setuptools import setup, find_packages

setup(
    name='queryengine',
    version='0.1.0',
    packages=find_packages(),
    entry_points ={
        'console_scripts': [
            'openit-queryengine = queryengine.__init__:run'
        ]
    },
    install_requires=[
        'aiohttp',
        'cchardet',
        'argparse'
    ]
)
